import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import VueKatex from "vue-katex";
import "katex/dist/katex.min.css";

Vue.config.productionTip = false;

new Vue({
  vuetify,
  render: (h) => h(App),
}).$mount("#app");

Vue.use(VueKatex, {
  globalOptions: {
    //... Define globally applied KaTeX options here
  },
});
