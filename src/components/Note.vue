<template>
  <v-container>
    <v-row class="text-center justify-center" wrap>
      <v-col cols="12">
        <h1 class="pa-10">
          A Gentle Introduction to Zero-Knowledge Proofs with Hands-On Examples
        </h1>
        <h2>Exploring Zcash Sapling Transactions</h2>
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        After having spent quite some time understanding the use of
        zero-knowledge proofs in the Zcash protocol, we thought that our
        insights might be of interest to others, too. On this website we
        illustratively explain the major steps in verifying a shielded
        transaction in Zcash to anyone familiar with Bitcoin’s basic mechanics.
        With the help of interactive examples, we let the reader experience what
        zero-knowledge proofs are.
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <p>
          <b>Why zero-knowledge proofs?</b> Well, that’s probably known by now,
          but this question serves as a good warm up… Bitcoin transactions are
          transparent allowing anybody to trace the movement of coins and their
          trajectories. To introduce anonymity, Zcash's spend and output
          descriptions use zero-knowledge (zk) proofs to hide information about
          the sender, recipients, and transaction amounts. We will shed some
          light on how you may verify in the current version of Zcash -- called
          Sapling -- that a transaction is valid without seeing neither any data
          about the wallet it originated from nor to whom and how much money was
          sent.
        </p>
        <p>
          <b>SNARKs:</b> Zk-SNARKs are special zk proofs (more precisely,
          arguments, but that’s a technical subtlety). The S of the acronym
          stands for succinct, i.e. the proofs are small in size, and the
          runtime for proof generation and verification is short. The N stands
          for non-interactive which means that in this proof system the prover
          can convince the verifier to know a secret with just one message. Both
          properties, S and N, are very useful, particularly for blockchains. J.
          Groth developed one of the most efficient
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://eprint.iacr.org/2016/260.pdf"
            >zk-SNARK proposals</a
          >
          to date, and is therefore used in Zcash Sapling transactions.
        </p>
        <p>
          This article is divided into two parts. In the first part -- written
          in a simpler language -- we set the focus on only one statement of the
          Sapling spend description: existence of a note. For this particular
          spend statement we will exemplarily generate a proof for a secret
          value of your choice. We then run the verification algorithm with the
          option to manipulate the input data to showcase that only with valid
          data the output is "true".
        </p>
        <p>
          The second part describes all data of the spend description without
          elaborate commentary, going quite fast through many difficult
          cryptographic tools. To provide further readings regarding these
          processes, we give helpful links to already existing introductions or
          literature covering these topics in more detail. In the latter
          section, you are also invited to load the spend description data of a
          real Sapling transaction and to get an idea of why the verification
          was indeed successful.
        </p>
        <v-row justify="center">
          <v-col cols="4">
            <b>Part 1</b>
            <p>
              What are notes and note commitment trees? How does a zk proof
              statement look like?
            </p>
            <p>
              <i>Practical example</i>: Create a note and generate a zk proof
              for its existence.
            </p>
          </v-col>
          <v-col cols="4">
            <b>Part 2</b>
            <p>
              What are SNARKs? What are spend descriptions and how they are
              verified?
            </p>
            <p>
              <i>Practical example</i>: Choose a Sapling tx ID and get a glimpse
              of what happens behind the scene.
            </p>
          </v-col>
        </v-row>
        <Separator />
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <h2>Proving Existence of a Note in Zero-Knowledge</h2>
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <h3>Notes and Note Commitments</h3>
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        For better understanding, we may imagine notes simply as currencies
        transferred during transactions even though this characterization is
        inaccurate in some ways. During a transaction, the original note of a
        sender becomes “invalidated" (by inserting it into the list of spent
        notes) and transformed into several new notes that are then issued to
        the encrypted recipients’ addresses. Of course, the value of the newly
        generated notes may not exceed the value of the spent note (minus the
        transaction fees charged by the miners).
        <p></p>
        Since Sapling does not make plain notes publicly readable for reasons of
        anonymity, they are "hidden”. To avoid clandestine manipulations of
        notes, e.g. increasing the note value to favor the sender, however, the
        plain notes may not be altered. This is achieved by cryptographic
        functions, so-called commitments, which can hide the note data and bind
        the content to the commitment not allowing any alterations afterwards.
        Although never revealed, the sender must prove knowledge of the plain
        note data by providing evidence that (s)he has a commitment trapdoor -
        something like a key to open the commitment.
        <p></p>
        Here comes a slightly incomplete definition of the note data structure
        (for more details see
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://github.com/zcash/zips/blob/master/protocol/protocol.pdf"
          >Zcash Protocol Specification</a
        >, 3.2): a note <span v-katex="'n'" /> in Sapling consists of
        <p></p>
        <ol>
          <li>
            The value
            <span v-katex="'\\mathrm{v}'" /> of the note <span v-katex="'n'" />.
          </li>
          <li>
            The shielded payment address
            <span v-katex="'\\mathrm{pk}'" /> to which the note was generated in
            a previous output description.
          </li>
          <li>
            The commitment trapdoor
            <span v-katex="'\\mathrm{rcm}'" /> of the note
            <span v-katex="'n'" />.
          </li>
        </ol>
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <h3>Note Commitment Tree and Valid Merkle Path</h3>
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        As already mentioned, transactions "consume" existing notes and generate
        new ones. To control this process efficiently, note commitments are
        organized in
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://en.wikipedia.org/wiki/Merkle_tree"
          >Merkle trees</a
        >. But unlike their use in Bitcoin, Zcash’s Merkle trees are
        incremental, i.e. of a fixed depth, but which can be incremented
        horizontally with new notes created in transaction outputs. These trees
        are called note commitment trees, and their Merkle roots ("anchors")
        change whenever new notes are added in the “still-to-be-filled“ leafs.
        <v-img :src="require('../assets/CommitmentTree-Clarito.jpg')" contain />
        <p class="caption text-center">
          Note commitment tree (inspiring source: Zcash Protocol Spec)
        </p>
        <p>
          A valid Merkle path in this tree, i.e. a path of hashes from the leaf
          containing the note commitment you want to spend up to the anchor,
          proves that this leaf belongs to the tree. It is therefore a note that
          has previously been added to the commitment tree in an output
          description, and hence can be spent.
        </p>
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <h3>Zero-knowledge Version of a Valid Merkle Path</h3>
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <p>
          To prove knowledge of a spendable note in “zero-knowledge” requires to
          prove knowledge of a note whose commitment is a leaf of a commitment
          tree without pointing to the specific leaf nor revealing any data of
          this note. Actually, there are two things to prove: knowing a note
          commitment and that this note commitment has a valid Merkle path. This
          is a composable zk proof, i.e. zk proofs with hiding intermediary
          value. Namely, the proof statement says that the spender knows a note
          <span v-katex="'n'" />, a commitment
          <span v-katex="'\\mathrm{cm}'" />, its position
          <span v-katex="'\\mathrm{pos}'" /> and path
          <span v-katex="'\\mathrm{path}'" /> in the commitment tree such that
          <span v-katex="'\\mathrm{cm} = \\mathrm{cm}'" />
          <span v-katex="'(n)'" />
          and
          <span v-katex="'\\mathrm{(cm}'" />,
          <span v-katex="'\\mathrm{pos}'" />,
          <span v-katex="'\\mathrm{path)}'" />
          form a valid Merkle path, i.e. roots in
          <span v-katex="'\\mathrm{rt}'" />. Here
          <span v-katex="'\\mathrm{cm}'" />
          is the note commitment, i.e. the result when applying the commitment
          function
          <span v-katex="'\\mathrm{cm()}'" />
          to the note
          <span v-katex="'n'" />, which is an intermediary between the note
          <span v-katex="'n'" />, and the Merkle path, and is not shown to the
          public! The spend description only provides the anchor
          <span v-katex="'\\mathrm{rt}'" />
          of a note commitment tree together with a proof.
        </p>
        <Separator />
        <p>
          Before we dig deeper into this matter in the second part of this
          article, let us create a note and calculate the anchor of the
          commitment tree containing its note commitment. For this we have set
          up an empty test commitment tree using the code from the librustzcash
          library:
        </p>
        <pre><code style="all: unset;">
        // create note & commitment using given value
        let note = to
            .create_note(value, Fs::random(&mut rng), &JUBJUB)
            .unwrap();
        
        let cm = Node::new(note.cm(&JUBJUB).into_repr());

        // create empty commitment tree and add commitment
        let mut tree = CommitmentTree::new();
        tree.append(cm).unwrap();
        
        let witness = IncrementalWitness::from_tree(&tree);
        let merkle_path = witness.path().unwrap();

        let anchor = Fr::from(witness.root());

        // ... proof generation skipped here
       </code></pre>
        <p class="caption">
          We use a payment address <code>to</code> to which the new note
          belongs. <code>Fs</code> is the scalar field of the
          <code>JUBJUB</code> curve and <code>Fr</code> the field of the
          arithmetic circuit engine. <code>JUBJUB</code> contains pre-computed
          parameters for the Jubjub curve. You find links to the repositories at
          the bottom of this page and some math background is explained in part
          two.
        </p>
      </v-col>
      <v-col cols="12">
        Choose the note value, and let's execute this code. Enjoy this moment,
        you are printing money ;-)
      </v-col>
    </v-row>
    <v-row>
      <v-col cols="12" md="6">
        <v-form ref="form" v-model="noteFormValid">
          <v-text-field
            v-model="noteValue"
            :rules="noteValueRules"
            label="note value in zat"
            placeholder="500"
            type="number"
            required
          ></v-text-field>
        </v-form>
      </v-col>
      <v-col>
        <v-btn
          :disabled="!noteFormValid || loading"
          color="success"
          outlined
          class="mr-4"
          @click="createNote"
          >Create Note</v-btn
        >
      </v-col>
    </v-row>

    <v-row v-if="loading" justify="center">
      <v-img :src="require('../assets/periscope.gif')" max-width="13%" />
    </v-row>

    <v-row v-if="loading" justify="center">
      <p>
        Note generation is fast, proof generation takes (only!) a few seconds
        ...
      </p>
    </v-row>
    <v-row v-if="errorMessage">
      <v-col>
        <h3>Can't reach server</h3>
        <p>
          Please try again later.
          {{ errorMessage }}
        </p>
      </v-col>
    </v-row>
    <div v-if="loadComplete">
      <NoteVerify v-bind:noteResults="noteResults" />
      <v-row>
        <v-col>
          <p>
            Without seeing any note data, the proof certifies that the note
            exists. Well, in this example we can also be convinced about its
            existence, since we created the note :).
          </p>
          <p>
            <b>Soundness property of SNARKs applied:</b> The soundness property
            of a proof system says that it is impossible to convince the
            verifier without knowing the correct secret value. So what if
            someone would come up with a proof that convinces the miners (s)he
            can spend more money, or that (s)he could spend a commitment note of
            the commitment tree that does not belong to her or him. This would
            be impossible. In the first case, (s)he cannot prove knowledge of a
            valid Merkle path, since the note commitment will change and
            collision-resistance of the commitment function ensures that it
            cannot appear as another existing leaf in the tree. Accordingly,
            there won’t be even any valid path to prove knowledge about it. In
            the latter case, (s)he does not know the note data
            <span v-katex="'n'" />, making it impossible to build a correctly
            validating proof showing that
            <span v-katex="'\\mathrm{cm}=\\mathrm{cm}(n)'" />. Here we must pay
            attention to the fact that the sender of the note knows the note
            content, although the note does not belong to her or him. Therefore,
            it is also required to prove knowledge of the spending key which is
            only known to the receiver, in order to make it impossible for the
            sender to claw back a sent note.
          </p>
          <p>
            The fact that it is impossible to correctly generate a validating
            proof without knowing the secret, has something to do with the roots
            of polynomials (sometimes referred to as “Arithmetization”). These
            polynomials are generated during the setup of the proof system. In
            the second part we will provide further readings about this process
            for those interested in understanding the full mechanism.
          </p>
        </v-col>
      </v-row>
    </div>
  </v-container>
</template>
<script>
const axios = require("axios");
import NoteVerify from "./NoteVerify";
import Separator from "./Separator";

export default {
  components: {
    NoteVerify,
    Separator,
  },
  data: () => ({
    noteValue: "500",
    noteValueRules: [
      (v) => {
        if (v === "0") return "we need a value greater than 0";
        var re = /^([0-9]{1,8})$/g;
        return re.test(v) || "Keep it real ;) choose a reasonable value";
      },
    ],
    noteFormValid: false,
    loadComplete: false,
    errorMessage: null,
    loading: false,
  }),
  methods: {
    createNote: function () {
      this.loadComplete = false;
      this.errorMessage = null;
      this.loading = true;
      axios
        .get(
          process.env.VUE_APP_SNARK_INTRO_SERVER +
            "create_note_and_proof/" +
            this.noteValue
        )
        .then((response) => {
          this.noteResults = response.data.proof_results;
          this.loadComplete = true;
        })
        .catch((error) => {
          // handle error
          this.errorMessage = error;
          console.log(error);
        })
        .finally(() => {
          this.loading = false;
        });
    },
  },
};
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped></style>
