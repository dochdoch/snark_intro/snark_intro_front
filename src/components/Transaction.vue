<template>
  <v-container>
    <v-row class="text-center justify-center" wrap>
      <v-col cols="12">
        <h1 class="pa-10">
          Part 2: Anatomy of Sapling Spend Descriptions
        </h1>
        <h2>The Use of Zero-Knowledge Proofs in Sapling</h2>
      </v-col>
    </v-row>
    <v-row>
      <v-col cols="12">
        <h2>Interlude</h2>
      </v-col>
    </v-row>
    <v-row>
      <v-col cols="12">
        This section illustrates from a very high-level perspective what the
        process of proof generation and proof verification includes. You may
        skip this part and go directly to the next section where things become
        more concrete again. Details are far beyond the scope of this article,
        and we will just point to already existing introductions which greatly
        explain these matters. Anyway, let’s walk through it taking the proof of
        knowledge of a note
        <span v-katex="'n'" /> which matches a commitment
        <span v-katex="'\\mathrm{cm}'" />
        as an example:
      </v-col>
    </v-row>
    <v-row>
      <v-col cols="12">
        <b>“Flattening” and Arithmetic Circuits:</b> There is a program "
        <span v-katex="'\\mathrm{cm}()'" />", a private input
        <span v-katex="'n'" />, and a public input
        <span v-katex="'\\mathrm{cm}'" />. The program "
        <span v-katex="'\\mathrm{cm}()'" />
        " must first be represented (or "flattened") as an arithmetic circuit.
        Imagine it as an “electric” circuit whose wires transport numbers of a
        finite field. At its gates the two incoming wires pass through an
        arithmetic operation (addition or multiplication) where the output of
        the gate is the result of the operation flowing upwards in the graph.
        The following picture shows how a possible arithmetic circuit may look
        like:
      </v-col>
    </v-row>
    <v-row class="text-center" justify="center" wrap>
      <v-col cols="12" md="8">
        <v-img :src="require('../assets/jubjub_anim.gif')" contain my-2 />
        <p class="caption text-center">
          Example of an arithmetic circuit.
        </p>
      </v-col>
    </v-row>
    <v-row>
      <v-col cols="12">
        Arithmetic circuits can represent any
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://en.wikipedia.org/wiki/NP_(complexity)"
          >NP statement</a
        >, and so also our <span v-katex="'\\mathrm{cm}()'" />. The input of the
        "
        <span v-katex="'\\mathrm{cm}()'" />
        ” are the commitment value
        <span v-katex="'\\mathrm{cm}'" /> and the secret input
        <span v-katex="'n'" />, and has only one output which is
        <span v-katex="'0'" />
        if
        <span v-katex="'\\mathrm{cm}=\\mathrm{cm}(n)'" />, else the output is
        <span v-katex="'1'" />.
      </v-col>
    </v-row>
    <v-row>
      <v-col cols="12">
        <b>Constraints:</b> A valid assignment of the input and output wires of
        all gates in the circuit form a constraint system -- a rank 1 constraint
        system or R1CS for short -- where the constraints are such that the left
        and right input of a gate gives the correct output value. See for
        example Vitalik's post
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://medium.com/@VitalikButerin/quadratic-arithmetic-programs-from-zero-to-hero-f6d558cea649"
          >1</a
        >, or this concrete
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://github.com/ebfull/bellman-demo/blob/master/src/main.rs"
          >example of XOR</a
        >.
      </v-col>
    </v-row>
    <v-row
      ><v-col cols="12">
        <b>SNARKs:</b> The R1CS of a given circuit helps build a set of
        polynomials with a special divisibility properties, so special that one
        may only achieve that the divisibility holds if one knows the secret
        value input. And it is the verifier's job to prove this property. The
        polynomials form a so-called quadratic arithmetic program (QAP) which is
        used to build SNARKs (cf. the verifying equation below as part of it).
        See
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://z.cash/technology/zksnarks/"
          >What are zk-SNARKs?</a
        >
        from Zcash, and again Vitalik's posts
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://medium.com/@VitalikButerin/quadratic-arithmetic-programs-from-zero-to-hero-f6d558cea649"
          >1</a
        >,
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://medium.com/@VitalikButerin/exploring-elliptic-curve-pairings-c73c1864e627"
          >2</a
        >,
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://medium.com/@VitalikButerin/zk-snarks-under-the-hood-b33151a013f6"
          >3</a
        >.
      </v-col>
    </v-row>
    <v-row
      ><v-col cols="12">
        <p>
          This is very condensed information, but as an important essence of it
          let’s keep in mind:
        </p>
        <ol>
          <li>
            It is not trivial at all to get even started with the first part:
            “flattening” a program you want to use in zk-SNARKs may be already a
            high entry hurdle. But the good news is that there are domain
            specific languages (DSL) available for some proof systems helping in
            achieving this, e.g. ZoKrates for libsnark. See the list of
            libraries for writing circuits
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://zkp.science/"
              >here</a
            >.
          </li>
          <li>
            Arithmetic circuits naturally fit better with arithmetic functions:
            E.g. hash functions of arithmetic flavor are more efficient. This
            can be illustrated by comparing the SHA256 with a Pedersen hash
            function over an elliptic curve (used in Sapling; see
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://github.com/zcash/zips/blob/master/protocol/protocol.pdf"
              >Zcash Protocol Specification</a
            >, 5.1.4.7): SHA256 has 27000 constraints while Pedersen has only
            1369 constraints. For the "efficiency measure" one takes the number
            of constraints of the R1CS.
          </li>
        </ol>
      </v-col>
    </v-row>
    <v-row
      ><v-col cols="12">
        Remark 2 lead to the choice of “arithmetic” functions in Sapling for
        hashes and commitments -- Pedersen Hash and Pedersen Commitment (a
        version of the
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="http://perso.ens-lyon.fr/benoit.libert/cours-ZK.pdf"
          >Pedersen commitment</a
        >
        ), which are both instantiated over the <i>Jubjub</i> curve. This is a
        twisted Edwards curve, and its name refers to the bird appearing in
        Lewis Carroll's story "The Hunting of the Snark". Jubjub is on the cover
        page of the Zcash spec and also appears on this website! The equation of
        the
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://z.cash/technology/jubjub/"
          >Jubjub</a
        >
        curve is
        <p></p>
        <p class="text-center">
          <span v-katex="' Ed: \-x^2 + y^2 = 1+ d x^2 y^2'" /> with
          <span v-katex="'\d =- 10240/10241'" />
        </p>
        <p>
          where the coefficients are from the finite field
          <span v-katex="'\\mathbb{F}_r'" /> ( <span v-katex="'r'" /> some
          255-bit prime). The hashes and commitments mentioned in the first part
          are all instantiated over this curve. Its defining field will greatly
          fit to another elliptic curve we will encounter later on allowing
          efficient circuit constructions.
        </p>
        <Separator />
      </v-col>
    </v-row>
    <h2>Sapling Spend Description</h2>
    <v-row>
      <v-col v-katex:auto>
        <p>
          If you enter the transaction ID of a Sapling transaction and press the
          load button, the first spend description of this transaction will be
          displayed. If you do not have any Sapling transaction at hand, you may
          use the prefilled transaction ID (check a Zcash block explorer to find
          other tx IDs, e.g.
          <a target="_blank" href="https://blockchair.com/zcash">Blockchair</a
          >):
        </p>
      </v-col>
    </v-row>
    <v-row>
      <v-col cols="12" md="6">
        <v-form ref="form" v-model="valid">
          <v-text-field
            v-model="txId"
            :rules="txIdRules"
            :counter="64"
            label="Tx ID"
            placeholder="5a1cbfb1..."
            required
          ></v-text-field>
        </v-form>
      </v-col>
      <v-col>
        <v-btn
          :disabled="!valid || loading"
          color="success"
          outlined
          class="mr-4"
          @click="loadTxRaw"
          >Load Tx</v-btn
        >
      </v-col>
    </v-row>
    <v-row v-if="loading" justify="center">
      <v-img :src="require('../assets/periscope.gif')" max-width="13%" />
    </v-row>

    <v-row v-if="loading" justify="center">
      <p>
        Loading transaction data ...
      </p>
    </v-row>
    <v-row v-if="txNotFound">
      <v-col>
        <h3>Transaction not found!</h3>
        <p>Can not find transaction for given tx id. Try another one ;-)</p>
      </v-col>
    </v-row>
    <v-row v-if="errorMessage">
      <v-col>
        <h3>Can't reach server</h3>
        <p>
          Please try again later.
          {{ errorMessage }}
        </p>
      </v-col>
    </v-row>
    <v-row v-if="notSappling">
      <v-col>
        <h3>Not a Sappling shielded transaction!</h3>
        <p>
          Please use a valid transaction ID for a Sappling transaction. All
          recent shielded Zcash transactions should work.
          <v-textarea
            outlined
            filled
            rows="7"
            label="Raw tx data"
            :value="txRaw"
          ></v-textarea
          >The transaction raw hex representation should start with
          <code>0400008085202f89</code>.
        </p>
      </v-col>
    </v-row>
    <v-row v-if="noSpend">
      <v-col>
        <h3>No shielded spend description found!</h3>
        <p>
          We need at least one spend description in the transaction, so a
          transaction from transparent pool into shielded pool won't work for
          our purpose.
        </p>
      </v-col>
    </v-row>
    <div v-if="loadComplete">
      <v-row>
        <v-col>
          <p>
            Sapling transactions can have several spend and output descriptions
            which consume old notes and create new ones, respectively. In this
            case, the transaction has
            <code>{{ txDetails.shielded_spends.length }}</code>
            spend(s) and
            <code>{{ txDetails.output_len }}</code> output(s).
          </p>
        </v-col>
      </v-row>
      <v-row>
        <v-col>
          <v-row>
            <v-col>
              <h4 v-katex:auto>
                Value Commitment
                <span class="value-public" v-katex="'\\mathrm{cv}'" />
              </h4>
            </v-col>
            <v-col cols="6">
              <i>Your Tx data (hex)</i>
            </v-col>
          </v-row>

          <v-row>
            <v-col>
              <p>
                This is the commitment to the value of the first spent note in
                this transaction. Note value commitments are necessary to check
                that spends and outputs (plus fees) of a transaction sum up to
                zero without knowing the real amounts. This is verified in the
                balance check not explained here.
              </p>
              <p>
                The commitment is a point on the JubJub curve. You see that the
                commitment is hiding, i.e. you cannot deduce from this curve
                point the value of the spent note
                <span class="chars">😆</span>.
              </p>
            </v-col>
            <v-col cols="6">
              <div class="prelike">{{ firstSpend.cv }}</div>
            </v-col>
          </v-row>
          <h4>
            Anchor
            <span class="value-public" v-katex="'\\mathrm{rt}'" />
          </h4>
          <v-row>
            <v-col cols="6">
              <p>
                The anchor is used to convince that the note really exists
                without revealing which note commitment is "consumed".
              </p>
              <p>
                It is the Merkle root (255 bit) of the
                <i>note commitment tree</i> -- as we discussed already more
                extensively in the first part of this article.
              </p>
            </v-col>
            <v-col cols="6">
              <div class="prelike">{{ firstSpend.anchor }}</div>
            </v-col>
          </v-row>
          <h4>
            Nullifier
            <span class="value-public" v-katex="'\\mathrm{nf}'" />
          </h4>
          <v-row>
            <v-col>
              <p>
                The nullifier (256 bit) plays the role of a serial number and
                publishing it marks the note as "spent" by inserting it into the
                list of already spent notes. It protects against double
                spending, since there can't exist a transparent unspent set of
                transactions as in Bitcoin.
              </p>
              <p>
                The nullifier of a note is the hash value derived from its
                commitment and position in the note commitment tree.
              </p>
            </v-col>
            <v-col cols="6">
              <div class="prelike">{{ firstSpend.nullifier }}</div>
            </v-col>
          </v-row>
          <h4>
            Authentication key
            <span class="value-public" v-katex="'\\mathrm{rk}'" />
          </h4>
          <v-row>
            <v-col cols="6">
              <p>
                The authorization key is used for the authorization of the spend
                description. This key is derived from the spending key.
              </p>
              <v-img
                :src="require('../assets/twistedsnark.jpg')"
                contain
                class="py-6"
              />
              <p>
                The authorization key is a public key used for the RedDSA
                signatures on the JubJub curve (see
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://github.com/zcash/zips/blob/master/protocol/protocol.pdf"
                  >Zcash Protocol Specification</a
                >, 5.4.6) which is based on
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://ed25519.cr.yp.to/eddsa-20150704.pdf"
                  >EdDSA</a
                >.
              </p>
            </v-col>
            <v-col cols="6">
              <div class="prelike">{{ firstSpend.rk }}</div>
            </v-col>
          </v-row>
          <h4>
            Spend Authorization Signature
            <span class="value-public" v-katex="'\\mathrm{SpendAuthSig}'" />
          </h4>
          <v-row>
            <v-col>
              <p>
                The authorization is used to prove knowledge of the spending key
                of the spent note.
              </p>
              <p>
                The authorization is the RedDSA signature
                <span v-katex="'(r, \s)'" /> of the transaction hash for the
                public authorization key <span v-katex="'\\mathrm{rk}'" />.
              </p>
            </v-col>
            <v-col cols="6">
              <div class="prelike">{{ firstSpend.spend_auth_sig }}</div>
            </v-col>
          </v-row>
          <h4>
            ZK proof
            <span class="value-public" v-katex="'\\pi_{spend}'" />
          </h4>
          <v-row>
            <v-col>
              <p>
                This is the zero-knowledge proof which verifies that the spend
                is correct. The proof consists of three points
                <span v-katex="'\(A,B,C)'" /> where
                <span v-katex="'\A= (A_x, A_y)'" /> and
                <span v-katex="'\C=(C_x,C_y)'" /> are points on the elliptic
                curve
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://electriccoin.co/blog/new-snark-curve/"
                  >BLS12-381</a
                >
                (see also
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://hackmd.io/@benjaminion/bls12-381"
                  >BLS12-381 for the rest of us</a
                >
                which provides further excellent references)
              </p>
              <p class="text-center">
                <span v-katex="'\E: y^2 = x^3 +4'" /> over the field
                <span v-katex="'\\mathbb{F}_q'" />
              </p>
              <p>
                with
                <span v-katex="'q'" /> some 381-bit prime, and
                <span v-katex="'\B=(B_x, B_y)'" /> lives on a sextic twist
              </p>
              <p>
                <span v-katex="'\E\': y^2 = x^3 + 4(u+1)'" /> over
                <span
                  v-katex="'\\mathbb{F}_{q^2} = \\mathbb{F}_q[u]/(u^2 +1)'"
                />
              </p>
              <p>
                where the coordinates are represented with two coefficients of a
                polynomial of degree one. In fact, the proof consists only of
                the compressed encoding of the
                <span v-katex="'x'" /> coordinates
                <span v-katex="'\(A_x,B_x,C_x)'" /> (see
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://github.com/zcash/zips/blob/master/protocol/protocol.pdf"
                  >Zcash Protocol Specification</a
                >, 5.4.8.2).
              </p>
            </v-col>
            <v-col cols="6">
              <div v-if="verifierLog[0]" class="prelike">
                { "Ax": "0x{{ verifierLog[0].proof_a_x }}"
                <br />
                "Bx": { "c0": "0x{{ verifierLog[0].proof_b_x_c0 }}"
                <br />
                "c1": "0x{{ verifierLog[0].proof_b_x_c1 }}" }
                <br />
                "Cx": "0x{{ verifierLog[0].proof_c_x }}" }
              </div>
            </v-col>
          </v-row>
        </v-col>
      </v-row>
      <v-row>
        <v-col v-katex:auto>
          <h2>What does the proof check?</h2>
          <p>
            For better understanding we use colors to differentiate between
            <span class="value-secret"> secret </span> values, known to the
            sender only, and <span class="value-public"> public </span> values,
            sent to the verifying miners.
          </p>
          <p>
            The proof statement of
            <span v-katex="'\\pi_{spend}'" /> says the following (in fact, a bit
            more; see
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://github.com/zcash/zips/blob/master/protocol/protocol.pdf"
              >Zcash Protocol Specification</a
            >, 4.15.2): The sender knows the note
            <span class="value-secret" v-katex="'n'" />, the
            <span class="value-secret" v-katex="'\\mathrm{path}'" /> and the
            position <span class="value-secret" v-katex="'\\mathrm{pos}'" /> of
            its commitment
            <span class="value-secret" v-katex="'\\mathrm{cm}'" /> in the
            commitment tree, and, of course, the spending key
            <span class="value-secret" v-katex="'\\mathrm{sk}'" />
            for the shielded payment address
            <span v-katex="'\\mathrm{pk}'" /> such that
          </p>
          <ol>
            <li>
              <i>Value commitment integrity</i>:
              <span class="value-public" v-katex="'\\mathrm{cv}'" />
              <span v-katex="'= \\mathrm{cm}'" />(
              <span class="value-secret" v-katex="'\\mathrm{v}'" />), where
              <span v-katex="'\\mathrm{cm}()'" /> is the commitment function.
            </li>
            <li>
              <i>Note existence & commitment integrity</i>: the anchor
              <span class="value-public" v-katex="'\\mathrm{rt}'" /> and
              <span
                class="value-secret"
                v-katex="'(\\mathrm{cm}, \\mathrm{pos},\\mathrm{path})'"
              />
              form a valid Merkle path &
              <span class="value-secret" v-katex="'\\mathrm{cm}'" />
              <span v-katex="'=\\mathrm{cm}'" />(
              <span class="value-secret" v-katex="'n'" />
              ). This we have seen in greater detail in the first part!!
            </li>
            <li>
              <i>Nullifier integrity</i>:
              <span class="value-public" v-katex="'\\mathrm{nf}'" />
              <span v-katex="'= \\mathrm{PRF}'" />(
              <span class="value-secret" v-katex="'\\rho'" /> ) where
              <span v-katex="'\\mathrm{PRF}()'" /> is a pseudo random function
              and
              <span class="value-secret" v-katex="'\\rho'" />
              <span v-katex="'=\\mathrm{Hash}('" />
              <span
                class="value-secret"
                v-katex="'\\mathrm{cm}, \\mathrm{position}'"
              />).
            </li>
            <li>
              <i>Spending authority</i>:
              <span class="value-public" v-katex="'\\mathrm{rk}'" /> is the
              public key derived from the spending key
              <span class="value-secret" v-katex="'\\mathrm{sk}'" />.
            </li>
          </ol>
        </v-col>
      </v-row>
      <v-row>
        <v-col v-katex:auto>
          <h2>How does the verification work?</h2>
          <p>
            The verification algorithm takes the data
            <span
              class="value-public"
              v-katex="
                ' \\mathrm{cv}, \\mathrm{rt}, \\mathrm{nf}, \\mathrm{rk}'
              "
            />
            and the proof
            <span class="value-public" v-katex="'\\pi_{spend}'" /> from the
            spend description as input. But it also requires the
            <span
              class="value-public"
              v-katex="'\\mathrm{verifying \\,\\, key}'"
            />
            which is part of the Common Reference String (CRS) generated during
            the initial setup of the SNARK allowing to generate and verify
            proofs.
          </p>
          <p>
            The verifying key consists of elements of the BLS12-381 curve \(E\)
            and its twist \(E'\). During the setup random
            <span
              v-katex="
                '\\alpha, \\beta, \\gamma, \\delta, x \\in \\mathbb{F}_r'
              "
            />
            are sampled ("toxic waste") which are then hidden in the exponents
            of elements \(g_1\), \(g_2\) in \(E\) and \(E'\), respectively:
          </p>
          <p class="text-center">
            <span
              v-katex="
                '\\mathrm{vk}_1 : (\\alpha_1 := g_1^{\\alpha}, \\beta_1:= g_1^{\\beta}, \\gamma_1:=g_1^{\\gamma},\\{\\frac{\\beta_1 u_i(x)+\\alpha_1 v_i(x)+w_i(x)}{\\gamma_1}\\}^l_{i=0})  \\in E^{4+l}'
              "
            />
          </p>
          <p class="text-center">
            <span
              v-katex="
                '\\mathrm{vk}_2 : (β_2:= g_2^{\\beta}, γ_2:= g_2^{\\gamma}, δ_2:= g_2^{\\delta}) \\in E^{\\prime \\, 3}'
              "
            />
          </p>
          <p>
            (the appearing \(u_i,v_i,w_i\)'s are polynomials evaluated at \(x\)
            in the exponent of \(g_1\) coming from the QAP; see the above
            references for their meaning).
          </p>
          <p></p>
          <p>
            There is still one piece missing to "only" understand the
            ingredients of the verification algorithm, and this requires quite
            some knowledge about elliptic curves: the proof verification makes
            use of the optimal Ate pairing (see Vitalik
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://medium.com/@VitalikButerin/exploring-elliptic-curve-pairings-c73c1864e627"
              >2</a
            >, or
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://crypto.stanford.edu/pbc/notes/ep/optimize.html"
              >lecture notes</a
            >
            ), a bilinear map into a finite field (a degree \(12\) extension of
            \(\mathbb{F}_r\) -- the \(12\) in BLS12-381))
          </p>
          <p class="text-center">
            <span
              v-katex="
                '\e: \G_1 \\times \G_2 \\longrightarrow \G_T=(\\mathbb{F}_{r^{12}})^{*}'
              "
            />,
          </p>
          <p>
            where \(G_1, G_2\) are subgroups of \(E,E'\) of order \(r\),
            respectively. This is the \(r\) of the defining field of the Jubjub
            curve, and whose fit makes the arithmetic circuits much smaller.
          </p>
          <p>
            <b>Verifying Equation:</b> Finally, writing the input from the spend
            description
            <span
              v-katex="'\\mathrm{cv}, \\mathrm{rt}, \\mathrm{nf}, \\mathrm{rk}'"
            />
            as
            <span v-katex="'\a_0, \\ldots \a_l \\in \\mathbb{F}_r'" />, the
            output of the verification algorithm is "true", depending whether
            the following equation in the finite field
            <span v-katex="'\\mathbb{F}_{r^{12}}'" /> holds true (compare with
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://eprint.iacr.org/2016/260.pdf"
              >Groth16</a
            >):
          </p>
          <p class="text-center">
            <span
              v-katex="
                'e(A,B) + e(\\sum_{i=0}^l a_i \\frac{\\beta_1 u_i(x) + \\alpha_1 v_i(x) +  w_i(x)}{\\gamma_1}, - \\gamma_2) + e(C, - \\delta_2)= e(\\alpha_1, \\beta_2)'
              "
            />
          </p>
          <p>
            Let's check by logging the executed code ... (the left column
            displays first the data before the pairing, and may then be compared
            with the data on the righthand side of the equation when the pairing
            is already applied to both sides).
          </p>
          <v-row>
            <v-col cols="6">
              Left side
              <div v-if="verifierLog[2]" class="prelike">
                "A": "0x{{ verifierLog[2].a }}"
                <br />
                "Acc": "0x{{ verifierLog[2].acc }}"
                <br />
                "B": "0x{{ verifierLog[2].b }}"
                <br />
                "Left side result": "{{ verifierLog[2].left_side_result }}"
              </div>
            </v-col>

            <v-col cols="6">Right side</v-col>
          </v-row>
          <v-row>
            <v-col cols="6">
              <div v-if="verifierLog[2]" class="prelike">
                "Left side result": "{{ verifierLog[2].left_side_result }}"
              </div>
            </v-col>
            <v-col cols="6">
              <div v-if="verifierLog[2]" class="prelike">
                "Alpha_g1_Beta_g2": "0x{{
                  verifierLog[2]["pvk.alpha_g1_beta_g2"]
                }}"
              </div>
            </v-col>
          </v-row>
          <p>
            Of course, for this Sapling transaction the equation holds
            <span class="chars">🥳</span>!
          </p>
          <p>
            If you are still with us, then we are happy that you made it through
            this jungle of formulas. Still you may wonder why this would suffice
            as a verification of a transaction, … and yes, you are right, there
            are more things the protocol must check: e.g. output description for
            note generation and the balance check are missing in our
            illustration. But to show the use of zero-knowledge proofs in
            Sapling, our introduction gives already a solid idea of how they are
            used.
          </p>
        </v-col>
      </v-row>
      <v-row>
        <v-col>
          Many thanks to D. Hopwood for corrections and comments on the original
          text.
        </v-col>
      </v-row>
      <v-row>
        <v-col>
          <h3>Reference</h3>
          <ul>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://github.com/zcash/zips/blob/master/protocol/protocol.pdf"
              >
                S. Bowe, D. Hopwood, T. Hornby, N. Wilcox: Zcash Protocol
                Specification. Version 20.04.2020.
              </a>
            </li>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://eprint.iacr.org/2016/260.pdf"
              >
                J. Groth: On the Size of Pairing-based Non-interactive
                Arguments. 2016.
              </a>
            </li>
          </ul>
          <br />
          <h3>Further Links</h3>
          <ul>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://medium.com/starkware/the-cambrian-explosion-of-crypto-proofs-7ac080ac9aed"
              >
                StarkWare (E. Ben-Sasson). The Cambrian Explosion of Crypto
                Proofs
              </a>
            </li>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://zkproof.org/"
                >ZKProof Standards</a
              >
            </li>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://zkp.science/"
                >zkp.science</a
              >
            </li>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://docs.rs/bellman/0.6.0/src/bellman/groth16/mod.rs.html#393-402"
                >Bellman crate for Groth16.</a
              >
            </li>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://link.springer.com/article/10.1007/s00145-018-9280-5"
              >
                An article about the security of pairing-based cryptography by
                Barbulescu and Duquesne.
              </a>
            </li>
          </ul>
        </v-col>
      </v-row>
    </div>
  </v-container>
</template>
<script>
const axios = require("axios");
import Separator from "./Separator";

export default {
  components: { Separator },
  data: () => ({
    txId: "78b8a456712378f2645a1cbfb1065ae3ce3da9fd70e5eb4af885916f86c5891d",
    txIdRules: [
      (v) => {
        var re = /^([0-9A-Fa-f]{64})$/g;
        return re.test(v) || "Tx ID has to be a hex string of length 64";
      },
    ],
    valid: false,
    txRaw: "not loaded yet",
    notSappling: false,
    noSpend: false,
    loadComplete: false, // load successfully terminated
    loading: false, // loading in progress
    txNotFound: false,
    errorMessage: null,
    txDetails: {
      shielded_spends: [
        {
          cv: "",
          anchor: "",
          nullifier: "",
          rk: "",
          spend_auth_sig: "",
          zkproof: "",
        },
      ],
      output_len: 0,
    },
    verifierLog: [],
    error: "",
  }),
  props: {
    msg: String,
  },
  computed: {
    firstSpend: function () {
      return this.txDetails.shielded_spends[0];
    },
  },
  methods: {
    loadTxRaw: function () {
      this.loading = true;
      this.loadComplete = false;
      this.noSpend = false;
      this.notSappling = false;
      this.txNotFound = false;
      this.errorMessage = null;
      let txId = this.txId;
      axios
        .get("https://api.blockchair.com/zcash/raw/transaction/" + txId)
        .then((response) => {
          if (response.data.data.length == 0) {
            this.txNotFound = true;
            return;
          }
          this.txRaw = response.data.data[txId]["raw_transaction"];
          if (this.txRaw.startsWith("0400008085202f89")) {
            this.loadTxDetails();
          } else {
            this.notSappling = true;
          }
        })
        .catch((error) => {
          // handle error
          this.errorMessage = error;
          console.log(error);
        })
        .finally(() => {
          this.loading = false;
        });
    },
    loadTxDetails: function () {
      this.loading = true;
      axios
        .post(process.env.VUE_APP_SNARK_INTRO_SERVER + "tx_details", {
          raw_tx: this.txRaw,
        })
        .then((response) => {
          this.txDetails = response.data.tx;
          if (this.txDetails.shielded_spends.length == 0) {
            this.noSpend = true;
            return;
          }
          this.verifierLog = response.data.log;
          this.loadComplete = true;
        })
        .catch((error) => {
          // handle error
          this.errorMessage = error;
          console.log(error);
        })
        .finally(() => {
          this.loading = false;
        });
    },
  },
};
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped></style>
