# snark_intro_front

Frontend for [SNARK Intro](https://doch_doch.gitlab.io/snark_intro/snark_intro_front/).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
